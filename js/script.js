$(document).ready(function () {
    $('.menu-icon').on('click', function () {
        $('.nav').toggleClass('active');
        $(this).toggleClass('open');
    })

    $('.nav-item').on('click', function(){
        $('.nav').removeClass('active');
        $('.menu-icon').removeClass('open');
    });
});